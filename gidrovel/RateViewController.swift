//
//  RateViewController.swift
//  gidrovel
//
//  Created by ifau on 10/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var sendButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    
    var rateObject: RateObject!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationItem.title = rateObject.title
        rateObject.tableView = tableView
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return rateObject.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return rateObject.stars.count
        }
        else if section == 1
        {
            return rateObject.thumbs.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath) as! RateStarsCell
            let star = rateObject.stars[indexPath.row]
            
            cell.setRate(star.value)
            cell.titleLabel.text = star.title
            cell.category = star.category
            cell.delegate = rateObject
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! RateThumbCell
            let thumb = rateObject.thumbs[indexPath.row]
            
            cell.setRate(thumb.value)
            cell.titleLabel.text = thumb.title
            cell.category = thumb.category
            cell.delegate = rateObject
            
            return cell
        }
    }
    
    // MARK: - Actions
    
    @IBAction func sendButtonPressed(_ sender: AnyObject)
    {
        if let markString = rateObject.serializedString()
        {
            sendButton.isEnabled = false
            ServiceAPI.sharedInstance.sendRate(markString) { [unowned self] (success: Bool) -> () in
                
                self.sendButton.isEnabled = true
                if success
                {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Оценка не заполнена", preferredStyle: .alert)
            self.present(alertController, animated: true, completion: nil)
            
            let delay = 1.5 * Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)) { () -> Void in
                alertController.dismiss(animated: true, completion: nil)
            }
        }
    }
}
