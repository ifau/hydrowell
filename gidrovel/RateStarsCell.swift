//
//  RateStarsCell.swift
//  gidrovel
//
//  Created by ifau on 29/10/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

protocol RateStarsCellDelegate: class
{
    func didSetRate(_ rate: Int, category: String)
}

class RateStarsCell: UITableViewCell
{
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var titleLabel: UILabel!
    
    var category: String!
    var delegate: RateStarsCellDelegate!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        for (index, button) in buttons.enumerated()
        {
            let image = UIImage(named: "IconStar")?.withRenderingMode(.alwaysTemplate)
            button.setImage(image, for: UIControlState())
            button.tintColor = UIColor.black
            button.tag = index
            button.addTarget(self, action: #selector(RateStarsCell.buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    func buttonPressed(_ sender: UIButton)
    {
        setRate((sender.tag + 1))
        delegate.didSetRate((sender.tag + 1), category: category)
    }
    
    func setRate(_ rate: Int)
    {
        for button in buttons
        {
            button.tintColor = button.tag <= (rate - 1) ? UIColor.yellow : UIColor.black
        }
    }
}
