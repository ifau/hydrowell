//
//  EnterPhoneViewController.swift
//  gidrovel
//
//  Created by ifau on 07/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class EnterPhoneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var phone: String!
    var didPresentedModal: Bool = false
    
    @IBOutlet var nextButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        if let _ = AppDefaults.userToken()
        {
            navigationItem.title = "Смена номера"
            if let number = AppDefaults.phoneNumber()
            {
                phone = number
            }
            else
            {
                phone = "+7 "
            }
        }
        else
        {
            navigationItem.title = "Авторизация"
            phone = "+7 "
        }
        
        nextButton.isEnabled = phone.characters.count < 18 ? false : true
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 84
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let _cell = cell as! PhoneTextfieldCell
        _cell.phoneTextField.text = phone
        _cell.phoneTextField.textDidChangeBlock = { tx in
            
            let textField = tx! as UITextField
            
            self.phone = textField.text!
            if self.phone.characters.count == 18
            {
                textField.resignFirstResponder()
            }
            self.nextButton.isEnabled = self.phone.characters.count < 18 ? false : true
        }
    }
    
    // MARK: - Actions
    
    @IBAction func nextButtonPressed(_ sender: AnyObject)
    {
        AppDefaults.setPhoneNumber(phone)

        let numberWithoutFormat = Utilities.removeFormattingFromPhone(phone)
        
        nextButton.isEnabled = false
        ServiceAPI.sharedInstance.sendPhoneRequest(numberWithoutFormat) { [unowned self] (success: Bool, needConfirm: Bool) -> () in
            
            self.nextButton.isEnabled = true
            if success
            {
                if needConfirm
                {
                    self.performSegue(withIdentifier: "confirmPhoneSegue", sender: nil)
                }
                else
                {
                    if self.didPresentedModal
                    {
                        self.dismiss(animated: true, completion: nil)
                    }
                    else
                    {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
        }
    }
    
    // MARK: - Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "confirmPhoneSegue"
        {
            let vc = segue.destination as! ConfirmPhoneViewController
            vc.didPresentedModal = didPresentedModal
            vc.numberWithoutFormat = Utilities.removeFormattingFromPhone(phone)
        }
    }
}
