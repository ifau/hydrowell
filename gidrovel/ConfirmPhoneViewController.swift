//
//  ConfirmPhoneViewController.swift
//  gidrovel
//
//  Created by ifau on 07/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class ConfirmPhoneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var doneButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    
    var didPresentedModal: Bool = false
    var numberWithoutFormat: String!
    var code: String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationItem.title = "Подтверждение"
        doneButton.isEnabled = false
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 84
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let _cell = cell as! TextFieldCell
        _cell.textField.text = code
        _cell.textField.addTarget(self, action: #selector(ConfirmPhoneViewController.textFieldDidChangeValue(_:)), for: UIControlEvents.editingChanged)
    }
    
    // MARK: - Actions
    
    func textFieldDidChangeValue(_ sender: UITextField)
    {
        code = sender.text!
        doneButton.isEnabled = code.characters.count > 0 ? true : false
    }
    
    @IBAction func doneButtonPressed(_ sender: AnyObject)
    {
        doneButton.isEnabled = false
        ServiceAPI.sharedInstance.sendConfirmCodeRequest(numberWithoutFormat, code: code) { [unowned self] (success: Bool) -> () in
            
            self.doneButton.isEnabled = true
            if success
            {
                if self.didPresentedModal
                {
                    self.dismiss(animated: true, completion: nil)
                }
                else
                {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
}
