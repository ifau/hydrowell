//
//  AppDelegate.swift
//  gidrovel
//
//  Created by ifau on 06/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import YandexMobileMetrica
import KVNProgress
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        YMMYandexMetrica.activate(withApiKey: "40660")
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        prepare()
        if AppDefaults.userToken() == nil
        {
            perform(#selector(AppDelegate.showLoginController), with: nil, afterDelay: 0.1)
        }
        return true
    }
    
    func showLoginController()
    {
        let vc = window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneRootVC") as! UINavigationController
        let enterPhoneVC = vc.topViewController as! EnterPhoneViewController
        enterPhoneVC.didPresentedModal = true
        window?.rootViewController?.present(vc, animated: true, completion: nil)
    }
    
    func prepare()
    {
        // clean after old version
        if let _ = UserDefaults.standard.value(forKey: "phoneNumberText")
        {
            UserDefaults.standard.removeObject(forKey: "cityCodeText")
            UserDefaults.standard.removeObject(forKey: "phoneNumberText")
            UserDefaults.standard.removeObject(forKey: "Token")
            UserDefaults.standard.synchronize()
        }
        //

        UINavigationBar.appearance().barStyle = .default
        UINavigationBar.appearance().barTintColor = UIColor(red: 109/255.0, green: 181/255.0, blue: 229/255.0, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

        let config = KVNProgressConfiguration()
        config.isFullScreen = false
        config.minimumDisplayTime = 1.0
        config.minimumSuccessDisplayTime = 0.5
        config.minimumErrorDisplayTime = 3.0
        config.circleStrokeForegroundColor = UIColor(red: 56/255.0, green: 89/255.0, blue: 116/255.0, alpha: 1)
        config.statusColor = UIColor(red: 56/255.0, green: 89/255.0, blue: 116/255.0, alpha: 1)
        KVNProgress.setConfiguration(config)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let notificationsToken = Utilities.dataToHexString(deviceToken)
        AppDefaults.setNotificationsToken(notificationsToken)
        ServiceAPI.sharedInstance.setServiceNotifications()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

