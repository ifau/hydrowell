//
//  AboutTabusViewController.swift
//  gidrovel
//
//  Created by ifau on 07/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class AboutTabusViewController: UIViewController
{
    @IBOutlet var locationTextView: UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationItem.title = "О компании «Табус»"
        
        let locationText = "Приезжайте\nАдрес: 141090, Россия, Московская обл., г. Королёв, ул. Московская, дом 3, офис 16\nВремя работы: пн-пт 9:00-18:00"
        
        let attributedDescription = NSMutableAttributedString(string: locationText)
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: NSRange(location: 0, length: locationText.characters.count))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: locationText.characters.count))
        attributedDescription.addAttribute(NSLinkAttributeName, value: URL(string: "http://maps.apple.com/?ll=55.937982,37.862013&z=16")!, range: (locationText as NSString).range(of: "141090, Россия, Московская обл., г. Королёв, ул. Московская, дом 3, офис 16"))
        
        locationTextView.attributedText = attributedDescription
    }
}
