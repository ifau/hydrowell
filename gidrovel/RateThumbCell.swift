//
//  RateThumbCell.swift
//  gidrovel
//
//  Created by ifau on 30/10/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

protocol RateThumbCellDelegate: class
{
    func didSetThumbRate(_ rate: Int, category: String)
    func canChangeThumbRate() -> Bool
}

class RateThumbCell: UITableViewCell
{
    @IBOutlet var upButton: UIButton!
    @IBOutlet var downButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    
    var category: String!
    var delegate: RateThumbCellDelegate!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        let image1 = UIImage(named: "IconThumbUp")?.withRenderingMode(.alwaysTemplate)
        
        upButton.setImage(image1, for: UIControlState())
        upButton.tintColor = UIColor.black
        upButton.addTarget(self, action: #selector(RateThumbCell.upButtonPressed(_:)), for: .touchUpInside)
        
        let image2 = UIImage(named: "IconThumbDown")?.withRenderingMode(.alwaysTemplate)
        
        downButton.setImage(image2, for: UIControlState())
        downButton.tintColor = UIColor.black
        downButton.addTarget(self, action: #selector(RateThumbCell.downButtonPressed(_:)), for: .touchUpInside)
    }
    
    func upButtonPressed(_ sender: UIButton)
    {
        if delegate.canChangeThumbRate()
        {
            setRate(0)
            delegate.didSetThumbRate(0, category: category)
        }
    }
    
    func downButtonPressed(_ sender: UIButton)
    {
        if delegate.canChangeThumbRate()
        {
            setRate(1)
            delegate.didSetThumbRate(1, category: category)
        }
    }
    
    func setRate(_ rate: Int)
    {
        if rate < 0
        {
            upButton.tintColor = UIColor.black
            downButton.tintColor = UIColor.black
        }
        else if rate == 0
        {
            upButton.tintColor = UIColor.yellow
            downButton.tintColor = UIColor.black
        }
        else if rate > 0
        {
            upButton.tintColor = UIColor.black
            downButton.tintColor = UIColor.yellow
        }
    }
}
