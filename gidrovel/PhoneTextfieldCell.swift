//
//  PhoneTextfieldCell.swift
//  gidrovel
//
//  Created by ifau on 06/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import SHSPhoneComponent

class PhoneTextfieldCell: UITableViewCell
{

    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var phoneTextField: SHSPhoneTextField!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        phoneTextField.formatter.prefix = "+7 "
        phoneTextField.formatter.setDefaultOutputPattern("(###) ###-##-##")
    }
}
