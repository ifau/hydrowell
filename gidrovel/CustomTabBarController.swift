//
//  CustomTabBarController.swift
//  gidrovel
//
//  Created by ifau on 14/06/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        for item in self.tabBar.items!
        {
            item.image = item.selectedImage?.imageWithColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)).withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            item.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)], for: UIControlState())
            item.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        }
    }
}
