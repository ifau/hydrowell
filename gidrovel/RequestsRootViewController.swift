//
//  RequestsRootViewController.swift
//  gidrovel
//
//  Created by ifau on 06/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RequestsRootViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
//        navigationItem.title = "Заявки"
        
        let titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 160, height: 44))
        titleView.contentMode = .scaleAspectFit
        titleView.image = UIImage(named: "logo")
        self.navigationItem.titleView = titleView
    }
    
    // MARK: - UITableView Delegate
    
    fileprivate let sectionTitles = ["Отправка заявки"]//, "Статус последней заявки"]
    fileprivate let requestTitles = ["Консультация по оборудованию", "Вызвать специалиста", "Заказать расходные материалы", "Купить систему очистки воды"]
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return requestTitles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            cell.textLabel?.text = requestTitles[indexPath.row]
            //cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            cell.textLabel?.textColor = UIColor.white
            cell.tintColor = UIColor.white
            cell.accessoryView?.tintColor = UIColor.white
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return sectionTitles[section]
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        if let view = view as? UITableViewHeaderFooterView
        {
            view.backgroundView?.backgroundColor = UIColor.clear
            view.textLabel?.backgroundColor = UIColor.clear
            view.textLabel?.textColor = UIColor.white
        }
    }
    
    fileprivate var requestType: Int = 0
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            requestType = indexPath.row
            performSegue(withIdentifier: "SendRequestSegue", sender: nil)
        }
    }
    
    // MARK: - Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "SendRequestSegue"
        {
            let vc = segue.destination as! RequestsSendRequestViewController
            vc.requestType = requestType
        }
    }
}
