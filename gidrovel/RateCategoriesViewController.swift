//
//  RateCategoriesViewController.swift
//  gidrovel
//
//  Created by ifau on 29/10/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RateCategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationItem.title = "Оцените нас"
    }
    
    var rates: [RateObject]
    {
        get
        {
            var rates: [RateObject] = []
            
            do
            {
                let thumb1 = ThumbRate(title: "Будете ли Вы рекомендовать нас знакомым?", value: -1, category: "recommendation")
                let rate = RateObject(title: "Оценка компании", stars: [], thumbs: [thumb1], type: 0)
                rates.append(rate)
            }
            
            do
            {
                let star1 = StarRate(title: "Компетентность", value: 0, category: "competency")
                let star2 = StarRate(title: "Вежливость", value: 0, category: "politeness")
                let star3 = StarRate(title: "Полнота консультации", value: 0, category: "consultation")
                let star4 = StarRate(title: "Оперативность в работе", value: 0, category: "efficiency")
                let star5 = StarRate(title: "Готовность помочь в целом", value: 0, category: "help")
                let stars = [star1, star2, star3, star4, star5]
                let thumb1 = ThumbRate(title: "Премировать          Депремировать", value: -1, category: "summaryRate")
                let rate = RateObject(title: "Оценка менеджера", stars: stars, thumbs: [thumb1], type: 1)
                rates.append(rate)
            }
            
            do
            {
                let star1 = StarRate(title: "Качество работы", value: 0, category: "WorkQuality")
                let star2 = StarRate(title: "Вежливость", value: 0, category: "politeness")
                let star3 = StarRate(title: "Своевременность прибытия", value: 0, category: "arrive")
                let star4 = StarRate(title: "Уборка после работы", value: 0, category: "cleaning")
                let star5 = StarRate(title: "Готовность помочь в целом", value: 0, category: "help")
                let stars = [star1, star2, star3, star4, star5]
                let thumb1 = ThumbRate(title: "Премировать          Депремировать", value: -1, category: "summaryRate")
                let rate = RateObject(title: "Оценка мастера", stars: stars, thumbs: [thumb1], type: 2)
                rates.append(rate)
            }
            return rates
        }
    }
    
    // MARK: - UITableView Delegate
    
    fileprivate let row0Titles = ["Оценка компании", "Оценка менеджера", "Оценка мастера"]
    fileprivate let row1Titles = ["Написать директору"]
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
            case 0: return row0Titles.count
            case 1: return row1Titles.count
            default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        switch indexPath.section
        {
            case 0:
                cell.textLabel?.text = row0Titles[indexPath.row]
                //cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
                cell.textLabel?.textColor = UIColor.white
            case 1:
                cell.textLabel?.text = row1Titles[indexPath.row]
                //cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
                cell.textLabel?.textColor = UIColor.white
            default: break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            performSegue(withIdentifier: "rateSegue", sender: nil)
        }
        else if indexPath.section == 1
        {
            performSegue(withIdentifier: "appealToDirectorSegue", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "rateSegue"
        {
            let index = tableView.indexPathForSelectedRow!.row
            let dvc = segue.destination as! RateViewController
            dvc.rateObject = rates[index]
        }
    }
}
