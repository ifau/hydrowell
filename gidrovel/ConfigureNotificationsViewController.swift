//
//  ConfigureNotificationsViewController.swift
//  gidrovel
//
//  Created by ifau on 20/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class ConfigureNotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate
{
    @IBOutlet var tableView: UITableView!
    fileprivate var period: Int = 19
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationItem.title = "Настройка напоминаний"
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return section == 0 ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return indexPath.section == 0 ? 200 : 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = indexPath.section == 0 ? "Cell0" : "Cell1"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            let _cell = cell as! PickerViewCell
            _cell.pickerView.dataSource = self
            _cell.pickerView.delegate = self
            _cell.pickerView.selectRow(period, inComponent: 0, animated: true)
        }
        else
        {
            cell.textLabel?.text = indexPath.row == 0 ? "Установить" : "Удалить напоминания"
            cell.textLabel?.textColor = indexPath.row == 0 ? UIColor.white : UIColor(red: 1.0, green: 0, blue: 0, alpha: 0.8)
            cell.textLabel?.textAlignment = .center
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1 && indexPath.row == 0
        {
            setNotifications()
        }
        else if indexPath.section == 1 && indexPath.row == 1
        {
            removeNotifications()
        }
    }
    
    // MARK: - UIPickerView Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return 60
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString?
    {
        let string = "\(row + 1) дней"
        return NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName:UIColor.white])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        period = row
    }
    
    // MARK: - Actions
    
    func setNotifications()
    {
        ServiceAPI.sharedInstance.setSaltNotifications(period + 1) { [unowned self] (success: Bool) -> () in
            
            if success
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    func removeNotifications()
    {
        ServiceAPI.sharedInstance.removeSaltNotifications { [unowned self] (success: Bool) -> () in
            
            if success
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}
