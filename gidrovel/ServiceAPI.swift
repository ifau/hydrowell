//
//  ServiceAPI.swift
//  gidrovel
//
//  Created by ifau on 09/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import KVNProgress

class ServiceAPI: NSObject
{
    static let sharedInstance = ServiceAPI()
    
    fileprivate var requestsCount : Int = 0
    {
        didSet
        {
            UIApplication.shared.isNetworkActivityIndicatorVisible = requestsCount > 0 ? true : false
        }
    }
    
    fileprivate func sendGetRequest(_ urlString: String, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ())
    {
        let session = URLSession.shared
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        let authData = "app:6Y4ARWBveihn".data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: []))"
        
        request.httpMethod = "GET"
        request.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        requestsCount = requestsCount + 1
        
        let task = session.dataTask(with: request, completionHandler: { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.requestsCount = self.requestsCount - 1
            DispatchQueue.main.async(execute: { completion(data, response, error) })
        }) 
        task.resume()
    }
    
    fileprivate func sendPostRequest(_ urlString: String, dataString: String, contentType: String, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ())
    {
        let session = URLSession.shared
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        let authData = "app:6Y4ARWBveihn".data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: []))"
        
        request.httpMethod = "POST"
        request.httpBody = dataString.data(using: String.Encoding.utf8)!
        request.setValue(authValue, forHTTPHeaderField: "Authorization")
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        requestsCount = requestsCount + 1
        
        let task = session.dataTask(with: request, completionHandler: { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.requestsCount = self.requestsCount - 1
            DispatchQueue.main.async(execute: { completion(data, response, error) })
        }) 
        task.resume()
    }
    
    fileprivate func sendPutRequest(_ urlString: String, dataString: String, contentType: String, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ())
    {
        let session = URLSession.shared
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        let authData = "app:6Y4ARWBveihn".data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: []))"
        
        request.httpMethod = "PUT"
        request.httpBody = dataString.data(using: String.Encoding.utf8)!
        request.setValue(authValue, forHTTPHeaderField: "Authorization")
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        requestsCount = requestsCount + 1
        
        let task = session.dataTask(with: request, completionHandler: { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.requestsCount = self.requestsCount - 1
            DispatchQueue.main.async(execute: { completion(data, response, error) })
        }) 
        task.resume()
    }
    
    fileprivate func sendDeleteRequest(_ urlString: String, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ())
    {
        let session = URLSession.shared
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        let authData = "app:6Y4ARWBveihn".data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: []))"
        
        request.httpMethod = "DELETE"
        request.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        requestsCount = requestsCount + 1
        
        let task = session.dataTask(with: request, completionHandler: { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.requestsCount = self.requestsCount - 1
            DispatchQueue.main.async(execute: { completion(data, response, error) })
        }) 
        task.resume()
    }
    
    // MARK: - API Actions
    
    func sendPhoneRequest(_ phoneWithoutFormatting: String, completion: @escaping (_ success: Bool, _ needConfirm: Bool) -> ())
    {
        KVNProgress.show()
        sendGetRequest("http://195.209.38.2/hw_app/hs/api/checkuser/get?param=\(phoneWithoutFormatting)")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    if let token = responseDictionary.value(forKey: "Token") as? String
                    {
                        KVNProgress.dismiss()
                        AppDefaults.setUserToken(token)
                        completion(true, false)
                    }
                    else if let state = responseDictionary.value(forKey: "State") as? Int, Int(state) == 2
                    {
                        KVNProgress.dismiss()
                        completion(true, true)
                    }
                    else if let state = responseDictionary.value(forKey: "State") as? Int, Int(state) < 0
                    {
                        KVNProgress.showError(withStatus: "Некорректный формат телефона")
                        completion(false, false)
                    }
                    else
                    {
                        KVNProgress.showError(withStatus: "Сервер временно недоступен")
                        completion(false, false)
                    }
                }
                catch
                {
                    KVNProgress.showError(withStatus: "Сервер временно недоступен[2]")
                    completion(false, false)
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false, false)
            }
        }
    }
    
    func sendConfirmCodeRequest(_ phoneWithoutFormatting: String, code: String, completion: @escaping (_ success: Bool) -> ())
    {
        KVNProgress.show()
        sendGetRequest("http://195.209.38.2/hw_app/hs/api/confirmuser/get?param=\(phoneWithoutFormatting)&code=\(code)")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    if let token = responseDictionary.value(forKey: "Token") as? String
                    {
                        AppDefaults.setUserToken(token)
                        KVNProgress.dismiss()
                        completion(true)
                    }
                    else
                    {
                        KVNProgress.showError(withStatus: "Неверный код подтверждения")
                        completion(false)
                    }
                }
                catch
                {
                    KVNProgress.showError(withStatus: "Сервер временно недоступен")
                    completion(false)
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false)
            }
        }
    }
    
    func sendRequest(_ type: Int, message: String, completion: @escaping (_ success: Bool) -> ())
    {
        let phoneValue = Utilities.removeCountryCodeFromPhone(AppDefaults.phoneNumber()!)
        let typeValue = "\(type)"
        let descriptionValue = Utilities.encodeXMLUnacceptableCharacters(message)
        let platformValue = "iOS"
        
        let xmlData = ["<xml>", "<phone>\(phoneValue)</phone>", "<type>\(typeValue)</type>", "<description>\(descriptionValue)</description>", "<platform>\(platformValue)</platform>", "</xml>"].joined(separator: "")
        
        KVNProgress.show()
        sendPostRequest("http://195.209.38.2/hw_app/hs/app_processing/sendapp", dataString: xmlData, contentType: "application/xml")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                let httpResponse = response as! HTTPURLResponse
                
                if httpResponse.statusCode == 200
                {
                    KVNProgress.showSuccess(withStatus: "Заявка успешно отправлена")
                    completion(true)
                }
                else
                {
                    KVNProgress.showError(withStatus: "Не удалось отправить заявку")
                    completion(false)
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false)
            }
        }
    }
    
    func sendRate(_ rateString: String, completion: @escaping (_ success: Bool) -> ())
    {
        KVNProgress.show()
        sendPostRequest("http://195.209.38.2/hw_app/hs/api/mark/send", dataString: rateString, contentType: "application/json")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                let httpResponse = response as! HTTPURLResponse
                
                if httpResponse.statusCode == 200
                {
                    KVNProgress.showSuccess(withStatus: "Ваша оценка успешно отправлена")
                    completion(true)
                }
                else
                {
                    KVNProgress.showError(withStatus: "Не удалось отправить оценку")
                    completion(false)
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false)
            }
        }
    }
    
    func sendAppealToDirector(_ message: String, completion: @escaping (_ success: Bool) -> ())
    {
        let phoneValue = Utilities.removeCountryCodeFromPhone(AppDefaults.phoneNumber()!)
        let typeValue = "4"
        let descriptionValue = Utilities.encodeXMLUnacceptableCharacters(message)
        let platformValue = "iOS"
        
        let xmlData = ["<xml>", "<phone>\(phoneValue)</phone>", "<type>\(typeValue)</type>", "<description>\(descriptionValue)</description>", "<platform>\(platformValue)</platform>", "</xml>"].joined(separator: "")
        
        KVNProgress.show()
        sendPostRequest("http://195.209.38.2/hw_app/hs/app_processing/sendapp", dataString: xmlData, contentType: "application/xml")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                let httpResponse = response as! HTTPURLResponse
                
                if httpResponse.statusCode == 200
                {
                    KVNProgress.showSuccess(withStatus: "Ваше обращение успешно отправлено")
                    completion(true)
                }
                else
                {
                    KVNProgress.showError(withStatus: "Не удалось отправить оценку")
                    completion(false)
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false)
            }
        }
    }
    
    func sendExtendContractRequest(_ completion: @escaping (_ success: Bool) -> ())
    {
        let phoneValue = Utilities.removeCountryCodeFromPhone(AppDefaults.phoneNumber()!)
        let typeValue = "6"
        let descriptionValue = ""
        let platformValue = "iOS"
        
        let xmlData = ["<xml>", "<phone>\(phoneValue)</phone>", "<type>\(typeValue)</type>", "<description>\(descriptionValue)</description>", "<platform>\(platformValue)</platform>", "</xml>"].joined(separator: "")
        
        KVNProgress.show()
        sendPostRequest("http://195.209.38.2/hw_app/hs/app_processing/sendapp", dataString: xmlData, contentType: "application/xml")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                let httpResponse = response as! HTTPURLResponse
                
                if httpResponse.statusCode == 200
                {
                    KVNProgress.showSuccess(withStatus: "Заявка успешно отправлена")
                    completion(true)
                }
                else
                {
                    KVNProgress.showError(withStatus: "Не удалось отправить заявку. Попробуйте еще раз")
                    completion(false)
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false)
            }
        }
    }
    
    func getClientStatus(_ completion: @escaping (_ success: Bool, _ status: String?) -> ())
    {
        let token = AppDefaults.userToken()!
        sendGetRequest("http://195.209.38.2/hw_app/hs/api/status/get?param=\(token)")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    if let status = responseDictionary.value(forKey: "status") as? String
                    {
                        if let date = responseDictionary.value(forKey: "date") as? String
                        {
                            completion(true, "\(status):\n\(date)")
                        }
                        else
                        {
                            completion(true, status)
                        }
                    }
                    else
                    {
                        completion(false, nil)
                    }
                }
                catch
                {
                    completion(false, nil)
                }
            }
            else
            {
                completion(false, nil)
            }
        }
    }
    
    func setServiceNotifications()
    {
        if let notificationsToken = AppDefaults.notificationsToken()
        {
            do
            {
                let phoneValue = Utilities.removeCountryCodeFromPhone(AppDefaults.phoneNumber()!)
                let jsonDictionary = ["PushCode": notificationsToken, "ClientOS":1, "DeviceID": notificationsToken, "PhoneNumber": phoneValue, "PushType":1] as [String : Any]
                let data = try JSONSerialization.data(withJSONObject: jsonDictionary, options: [])
                let jsonString = String(data: data, encoding: String.Encoding.utf8)!

                sendPutRequest("http://hydrowellalerts.azurewebsites.net/api/Salt", dataString: jsonString, contentType: "application/json")
                { (data: Data?, response: URLResponse?, error: Error?) -> () in
                    
//                    if error == nil
//                    {
//                        do
//                        {
//                            let responseDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! NSDictionary
//                            if let item = responseDictionary.valueForKey("Item") as? Bool where item == true
//                            {
//                                
//                            }
//                            else
//                            {
//
//                            }
//                        }
//                        catch
//                        {
//                            
//                        }
//                    }
//                    else
//                    {
//
//                    }
                }
            }
            catch
            {
                
            }
        }
        else
        {

        }
    }
    
    func getSaltNotificationsStatus(_ completion: @escaping (_ success: Bool, _ status: String?) -> ())
    {
        if let notificationsToken = AppDefaults.notificationsToken()
        {
            sendGetRequest("http://hydrowellalerts.azurewebsites.net/api/Status?deviceID=\(notificationsToken)")
            { (data: Data?, response: URLResponse?, error: Error?) -> () in
                
                if error == nil
                {
                    do
                    {
                        //print("get: \(String(data: data!, encoding: NSUTF8StringEncoding)!)")
                        let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                        if let item = responseDictionary.value(forKey: "Item") as? NSDictionary, let period = item.value(forKey: "Period") as? Int, let daysLeft = item.value(forKey: "DaysLeft") as? Int
                        {
                            completion(true, "Период уведомлений \(period) дней, следующее уведомление через \(daysLeft) дней")
                        }
                        else
                        {
                            completion(true, "В данный момент напоминания о соли не установлены")
                        }
                    }
                    catch
                    {
                        completion(false, nil)
                    }
                }
                else
                {
                    completion(false, nil)
                }
            }
        }
        else
        {
            completion(true, "В данный момент напоминания о соли не установлены")
        }
    }
    
    func setSaltNotifications(_ period: Int, completion: @escaping (_ success: Bool) -> ())
    {
        if let notificationsToken = AppDefaults.notificationsToken()
        {
            do
            {
                let phoneValue = Utilities.removeCountryCodeFromPhone(AppDefaults.phoneNumber()!)
                let jsonDictionary = ["PushCode": notificationsToken, "Period":period, "ClientOS":1, "DeviceID": notificationsToken, "PhoneNumber": phoneValue, "PushType":0] as [String : Any]
                let data = try JSONSerialization.data(withJSONObject: jsonDictionary, options: [])
                let jsonString = String(data: data, encoding: String.Encoding.utf8)!
                // PUT PushType 1, period любой
                // 
                sendPostRequest("http://hydrowellalerts.azurewebsites.net/api/Salt", dataString: jsonString, contentType: "application/json")
                { (data: Data?, response: URLResponse?, error: Error?) -> () in
                    
                    if error == nil
                    {
                        do
                        {
                            let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                            if let item = responseDictionary.value(forKey: "Item") as? Bool, item == true
                            {
                                completion(true)
                            }
                            else
                            {
                                completion(false)
                                KVNProgress.showError(withStatus: "Не удалось установить напоминания о соли. Попробуйте еще раз")
                            }
                        }
                        catch
                        {
                            
                        }
                    }
                    else
                    {
                        completion(false)
                        KVNProgress.showError(withStatus: "Не удалось установить напоминания о соли. Попробуйте еще раз")
                    }
                }
            }
            catch
            {
                
            }
        }
        else
        {
            completion(false)
            KVNProgress.showError(withStatus: "Необходимо разрешить приложению отправку уведомлений в настройках телефона")
        }
    }
    
    func removeSaltNotifications(_ completion: @escaping (_ success: Bool) -> ())
    {
        if let notificationsToken = AppDefaults.notificationsToken()
        {
            KVNProgress.show()
            sendDeleteRequest("http://hydrowellalerts.azurewebsites.net/api/Salt?deviceID=\(notificationsToken)")
            { (data: Data?, response: URLResponse?, error: Error?) -> () in
                
                if error == nil
                {
                    do
                    {
                        let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                        if let item = responseDictionary.value(forKey: "Item") as? Bool, item == true
                        {
                            KVNProgress.dismiss()
                            completion(true)
                        }
                        else if let message = responseDictionary.value(forKey: "Message") as? String
                        {
                            KVNProgress.showError(withStatus: message)
                            completion(false)
                        }
                    }
                    catch
                    {
                        KVNProgress.showError(withStatus: "Не удалось удалить напоминания. Попробуйте еще раз")
                        completion(false)
                    }
                }
                else
                {
                    KVNProgress.showError(withStatus: "Не удалось удалить напоминания. Попробуйте еще раз")
                    completion(false)
                }
            }
        }
        else
        {
            KVNProgress.showError(withStatus: "В данный момент напоминания не установлены")
            completion(false)
        }
    }
}
