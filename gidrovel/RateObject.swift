//
//  RateObject.swift
//  gidrovel
//
//  Created by ifau on 29/10/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import Foundation

struct StarRate
{
    var title: String
    var value: Int
    var category: String
}

struct ThumbRate
{
    var title: String
    var value: Int
    var category: String
}

class RateObject
{
    var title: String!
    var stars: [StarRate]!
    var thumbs: [ThumbRate]!
    var type: Int!
    
    weak var tableView: UITableView!
    var didInsertThumbsSection: Bool = false
    
    init(title: String, stars: [StarRate], thumbs: [ThumbRate], type: Int)
    {
        self.title = title
        self.stars = stars
        self.thumbs = thumbs
        self.type = type
    }
    
    func numberOfSections() -> Int
    {
        if (stars.count > 0)
        {
            for star in stars
            {
                if star.value == 0
                {
                    return 1
                }
            }
        }
        return 2
    }
    
    func insertThumbSectionIfNeeded()
    {
        if (!didInsertThumbsSection && tableView != nil)
        {
            var needInsertSection = true
            for star in stars
            {
                if star.value == 0
                {
                    needInsertSection = false
                    break
                }
            }
            if needInsertSection
            {
                tableView.insertSections(IndexSet(integer: 1), with: .fade)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { [unowned self] () -> Void in
                    self.tableView.setContentOffset(CGPoint(x: 0, y: self.tableView.contentSize.height - self.tableView.frame.size.height), animated: true)
                }
                didInsertThumbsSection = true
            }
        }
    }
    
    func serializedString() -> String?
    {
        if (stars.count == 0 && thumbs.count == 1 && thumbs[0].value < 0)
        {
            return nil;
        }
        
        if (stars.count > 0)
        {
            var allStarsSet = true
            for star in stars
            {
                if star.value == 0
                {
                    allStarsSet = false
                    break
                }
            }
            if !allStarsSet
            {
                return nil
            }
        }
        
        var marks = Dictionary<String,AnyObject>()
        for star in stars
        {
            if star.value > 0
            {
                marks[star.category] = star.value as AnyObject?
            }
        }
        
        for thumb in thumbs
        {
            if thumb.value >= 0
            {
                marks[thumb.category] = thumb.value as AnyObject?
                
                // костыль, так как:
                // recommendation – integer; оценка компании, возможные значения: 0 – клиент не будет рекомендовать фирму, 1 – клиент будет рекомендовать фирму.
                // summaryRate – integer; премировать (0)/депримировать (1).
                if thumb.category == "recommendation"
                {
                    marks[thumb.category] = (thumb.value == 0 ? 1 : 0) as AnyObject
                }
            }
        }
        
        var dictionary = Dictionary<String,AnyObject>()
        dictionary["token"] = AppDefaults.userToken()! as AnyObject?
        dictionary["type"] = type as AnyObject?
        dictionary["platform"] = "iOS" as AnyObject?
        dictionary["marks"] = marks as AnyObject?
        
        let data = try! JSONSerialization.data(withJSONObject: dictionary, options: [])
        let string = String(data: data, encoding: String.Encoding.utf8)!
        return string
    }
}

extension RateObject: RateStarsCellDelegate
{
    func didSetRate(_ rate: Int, category: String)
    {
        for (index, star) in stars.enumerated()
        {
            if star.category == category
            {
                stars[index].value = rate
                break
            }
        }
        insertThumbSectionIfNeeded()
    }
}

extension RateObject: RateThumbCellDelegate
{
    func didSetThumbRate(_ rate: Int, category: String)
    {
        for (index, thumb) in thumbs.enumerated()
        {
            if thumb.category == category
            {
                thumbs[index].value = rate
                break
            }
        }
    }
    
    func canChangeThumbRate() -> Bool
    {
        var can = true
        
        for star in stars
        {
            if star.value == 0
            {
                can = false
                break
            }
        }
        
        return can
    }
}
