//
//  MoreRootViewController.swift
//  gidrovel
//
//  Created by ifau on 07/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class MoreRootViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
//        navigationItem.title = "Дополнительно"
        
        let titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 160, height: 44))
        titleView.contentMode = .scaleAspectFit
        titleView.image = UIImage(named: "logo")
        self.navigationItem.titleView = titleView
    }
    
    // MARK: - UITableView Delegate
    
    fileprivate let sectionTitles = ["Обратная связь", "Учетная запись", "Информация о компании", ""]
    fileprivate let row0Titles = ["Оцените нас"]
    fileprivate let row1Titles = ["Сменить номер телефона"]
    fileprivate let row2Titles = ["О компании «Гидровелл»"]
    fileprivate let row3Titles = ["Разработано в «Табус»"]
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
            case 0: return row0Titles.count
            case 1: return row1Titles.count
            case 2: return row2Titles.count
            case 3: return row3Titles.count
            default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        switch indexPath.section
        {
            case 0:
                cell.textLabel?.text = row0Titles[indexPath.row]
                cell.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.textAlignment = .left
            case 1:
                cell.textLabel?.text = row1Titles[indexPath.row]
                cell.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.textAlignment = .left
            case 2:
                cell.textLabel?.text = row2Titles[indexPath.row]
                cell.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.textAlignment = .left
            case 3:
                cell.textLabel?.text = row3Titles[indexPath.row]
                cell.backgroundColor = UIColor.clear
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.textAlignment = .center
            default: break
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let title = sectionTitles[section]
        return title.characters.count > 0 ? title : nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        if let view = view as? UITableViewHeaderFooterView
        {
            view.backgroundView?.backgroundColor = UIColor.clear
            view.textLabel?.backgroundColor = UIColor.clear
            view.textLabel?.textColor = UIColor.white
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0 && indexPath.row == 0
        {
            performSegue(withIdentifier: "ratecategorySegue", sender: nil)
        }
        else if indexPath.section == 1
        {
            performSegue(withIdentifier: "enterPhoneSegue", sender: nil)
        }
        else if indexPath.section == 2
        {
            performSegue(withIdentifier: "aboutHydrowellSegue", sender: nil)
        }
        else if indexPath.section == 3
        {
            performSegue(withIdentifier: "aboutTabusSegue", sender: nil)
        }
    }
}
