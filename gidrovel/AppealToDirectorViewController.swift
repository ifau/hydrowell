//
//  AppealToDirectorViewController.swift
//  gidrovel
//
//  Created by ifau on 10/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class AppealToDirectorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate
{
    @IBOutlet var sendButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    fileprivate var message = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationItem.title = "Письмо директору"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        sendButton.isEnabled = true
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let _cell = cell as! TextViewCell
        _cell.descriptionLabel.text = "Ваше обращение к директору компании"
        _cell.textView.text = message
        _cell.textView.delegate = self
    }
    
    // MARK: - Actions
    
    func textViewDidChange(_ textView: UITextView)
    {
        message = textView.text
    }
    
    @IBAction func sendButtonPressed(_ sender: AnyObject)
    {
        guard !message.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else
        {
            return
        }
        
        ServiceAPI.sharedInstance.sendAppealToDirector(message) { [unowned self] (success: Bool) -> () in
            
            if success
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}
