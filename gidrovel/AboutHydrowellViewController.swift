//
//  AboutHydrowellViewController.swift
//  gidrovel
//
//  Created by ifau on 07/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class AboutHydrowellViewController: UIViewController
{

    @IBOutlet var descriptionTextView: UITextView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationItem.title = "О компании «Гидровелл»"
        
        let descriptionText = "Компания «Гидровелл» основана в 1995 году. Мы специализируемся на эффективных решениях всех проблем, связанных с использованием воды в быту и на производстве.\n\n" +
        
        "Мы работаем по стандартам завтрашнего дня уже сегодня. «Гидровелл» — это оборудование лучшего качества, максимально широкий спектр услуг и профессионалы высшего уровня.\n\n" +
        
        "Монтаж, модернизация и сервисное обслуживание водоочистных систем  — мы предлагаем:\n\n" +
        
        " • Системы очистки воды ведущих мировых производителей;\n" +
        " • Бурение и обустройство скважин на воду;\n" +
        " • Насосы и сопутствующие оборудование Grundfos;\n" +
        " • Комплексные решения по водоснабжению объектов «под ключ»;\n" +
        " • Комплектация объектов;\n" +
        " • Проект → поставка → монтаж → гарантийное и сервисное обслуживание.\n\n" +
        
        "Отдел продаж: +7 (495) 120-01-41\n" +
        "Служба сервиса: +7 (495) 212-18-48\n\n" +
        
        "Веб-сайт: http://vodadoma.ru/\n" +
        "E-mail: info@vodadoma.ru\n\n" +
        
        "Адрес: 125195, Москва, Ленинградское ш., 96 А"
        
        let attributedDescription = NSMutableAttributedString(string: descriptionText)
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSLinkAttributeName, value: URL(string: "http://maps.apple.com/?ll=55.8273163,37.4785869&z=16")!, range: (descriptionText as NSString).range(of: "125195, Москва, Ленинградское ш., 96 А"))
        
        descriptionTextView.attributedText = attributedDescription
    }
}
