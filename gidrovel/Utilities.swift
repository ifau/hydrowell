//
//  Utilities.swift
//  gidrovel
//
//  Created by ifau on 17/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class Utilities: NSObject
{
    class func removeFormattingFromPhone(_ phone: String) -> String
    {
        // "from +7 (999) 888-77-66"
        // "to 9998887766"
        
        return phone.replacingOccurrences(of: "+7 (", with: "").replacingOccurrences(of: ") ", with: "").replacingOccurrences(of: "-", with: "")
    }
    
    class func removeCountryCodeFromPhone(_ phone: String) -> String
    {
        // "from +7 (999) 888-77-66"
        // "to (999) 888-77-66"
        
        return phone.replacingOccurrences(of: "+7 ", with: "")
    }
    
    class func encodeXMLUnacceptableCharacters(_ string: String) -> String
    {
        return string.replacingOccurrences(of: "&", with: "&amp;").replacingOccurrences(of: "\"", with: "&quot;").replacingOccurrences(of: "'", with: "&#39;").replacingOccurrences(of: ">", with: "&gt;").replacingOccurrences(of: "<", with: "&lt;")
    }
    
    class func dataToHexString(_ data: Data) -> String
    {
        var str: String = String()
        let p = (data as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.count)
        let len = data.count
        
        for i in 0 ..< len {
            str += String(format: "%02.2X", p[i])
        }
        return str
    }
}

extension UIImage
{
    func imageWithColor(_ color1: UIColor) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0);
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
