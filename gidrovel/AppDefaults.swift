//
//  AppDefaults.swift
//  gidrovel
//
//  Created by ifau on 10/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class AppDefaults: NSObject
{
    class func setPhoneNumber(_ phoneNumber: String)
    {
        UserDefaults.standard.setValue(phoneNumber, forKey: "PhoneNumber")
        UserDefaults.standard.synchronize()
    }
    
    class func phoneNumber() -> String?
    {
        return UserDefaults.standard.value(forKey: "PhoneNumber") as? String
    }
    
    class func setUserToken(_ token: String)
    {
        UserDefaults.standard.setValue(token, forKey: "Token")
        UserDefaults.standard.synchronize()
    }
    
    class func userToken() -> String?
    {
        return UserDefaults.standard.value(forKey: "Token") as? String
    }
    
    class func setNotificationsToken(_ token: String)
    {
        UserDefaults.standard.setValue(token, forKey: "NotificationsToken")
        UserDefaults.standard.synchronize()
    }
    
    class func notificationsToken() -> String?
    {
        return UserDefaults.standard.value(forKey: "NotificationsToken") as? String
    }
    
    class func setSaltNotificationsPeriod(_ period: Int)
    {
        UserDefaults.standard.set(period, forKey: "SaltNotificationsPeriod")
        UserDefaults.standard.synchronize()
    }
    
    class func saltNotificationsPeriod() -> Int
    {
        return UserDefaults.standard.integer(forKey: "SaltNotificationsPeriod")
    }
}
