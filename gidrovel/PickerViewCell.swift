//
//  PickerViewCell.swift
//  gidrovel
//
//  Created by ifau on 20/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class PickerViewCell: UITableViewCell
{

    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var pickerView: UIPickerView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
}
