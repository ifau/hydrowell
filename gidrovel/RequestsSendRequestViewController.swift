//
//  RequestsSendRequestViewController.swift
//  gidrovel
//
//  Created by ifau on 06/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RequestsSendRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate
{
    var requestType: Int!
    fileprivate var phone: String!
    fileprivate var message = ""
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var sendButton: UIBarButtonItem!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let number = AppDefaults.phoneNumber(), number.characters.count == 18
        {
            phone = number
        }
        else
        {
            phone = "+7 "
        }
        navigationItem.title = "Отправка заявки"
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 75
        }
        else
        {
            return 160
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell\(indexPath.row)", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let _cell = cell as! PhoneTextfieldCell
            _cell.phoneTextField.text = phone
            _cell.phoneTextField.isEnabled = false
        }
        else if indexPath.row == 1
        {
            let _cell = cell as! TextViewCell
            _cell.textView.text = message
            _cell.textView.delegate = self
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch requestType
        {
            case 0: return "Заявка на консультацию по оборудованию"
            case 1: return "Заявка на вызов специалиста"
            case 2: return "Заявка на заказ расходных материалов"
            case 3: return "Заявка на покупку системы очистки воды"
            default: return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        if let view = view as? UITableViewHeaderFooterView
        {
            view.backgroundView?.backgroundColor = UIColor.clear
            view.textLabel?.backgroundColor = UIColor.clear
            view.textLabel?.textColor = UIColor.white
            view.textLabel?.text = self.tableView(tableView, titleForHeaderInSection: 0)
        }
    }
    
    // MARK: - Actions
    
    func textViewDidChange(_ textView: UITextView)
    {
        message = textView.text
    }
    
    @IBAction func sendButtonPressed(_ sender: AnyObject)
    {
        sendButton.isEnabled = false
        ServiceAPI.sharedInstance.sendRequest(requestType, message: message) { [unowned self] (success: Bool) -> () in
            
            self.sendButton.isEnabled = true
            if success
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}
