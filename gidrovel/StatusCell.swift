//
//  StatusCell.swift
//  gidrovel
//
//  Created by ifau on 18/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class StatusCell: UITableViewCell
{
    @IBOutlet var loadingView: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var statusLabel: UILabel!
    
    var isLoadingState: Bool = false
    {
        didSet
        {
            mainView.isHidden = isLoadingState ? true : false
            loadingView.isHidden = isLoadingState ? false : true
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
}
