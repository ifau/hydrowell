//
//  ServiceRootViewController.swift
//  gidrovel
//
//  Created by ifau on 18/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class ServiceRootViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    
    fileprivate var clientStatus: String?
    fileprivate var notificationStatus: String?
    
    fileprivate var isSendClientStatusRequest: Bool = false
    fileprivate var isSendNotificationStatusRequest: Bool = false
    
    fileprivate var isSendExtendContract: Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
//        navigationItem.title = "Обслуживание"
        
        let titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 160, height: 44))
        titleView.contentMode = .scaleAspectFit
        titleView.image = UIImage(named: "logo")
        self.navigationItem.titleView = titleView
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if !isSendClientStatusRequest
        {
            sendClientStatusRequest()
        }

        if !isSendNotificationStatusRequest
        {
            sendNotificationsStatusRequest()
        }
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
        let _cell = cell as! StatusCell
        
        if indexPath.section == 0
        {
            _cell.isLoadingState = clientStatus == nil ? true : false
            _cell.actionButton.setTitle("Продлить договор", for: UIControlState())
            _cell.statusLabel.text = clientStatus
        }
        else if indexPath.section == 1
        {
            _cell.isLoadingState = notificationStatus == nil ? true : false
            _cell.actionButton.setTitle("Настроить", for: UIControlState())
            _cell.statusLabel.text = notificationStatus
        }
        
        _cell.actionButton.tag = indexPath.section
        _cell.actionButton.addTarget(self, action: #selector(ServiceRootViewController.buttonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        if let view = view as? UITableViewHeaderFooterView
        {
            view.backgroundView?.backgroundColor = UIColor.clear
            view.textLabel?.backgroundColor = UIColor.clear
            view.textLabel?.textColor = UIColor.white
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section
        {
            case 0: return "Статус клиента"
            case 1: return "Статус напоминаний о соли"
            default: return nil
        }
    }
    
    // MARK: - Actions
    
    func buttonPressed(_ sender: UIButton)
    {
        if sender.tag == 0 && !isSendExtendContract
        {
            sendExtendContractRequest()
        }
        else if sender.tag == 1
        {
            performSegue(withIdentifier: "configureNotificationsSegue", sender: nil)
        }
    }
    
    func sendClientStatusRequest()
    {
        isSendClientStatusRequest = true
        ServiceAPI.sharedInstance.getClientStatus { [unowned self] (success: Bool, status: String?) -> () in
            
            if success
            {
                self.clientStatus = status
                self.tableView.reloadSections(IndexSet(integer: 0), with: UITableViewRowAnimation.automatic)
            }
            
            self.isSendClientStatusRequest = false
        }
    }
    
    func sendNotificationsStatusRequest()
    {
        isSendNotificationStatusRequest = true
        ServiceAPI.sharedInstance.getSaltNotificationsStatus { [unowned self] (success: Bool, status: String?) -> () in
            
            if success
            {
                self.notificationStatus = status
                self.tableView.reloadSections(IndexSet(integer: 1), with: UITableViewRowAnimation.automatic)
            }
            
            self.isSendNotificationStatusRequest = false
        }
    }
    
    func sendExtendContractRequest()
    {
        isSendExtendContract = true
        ServiceAPI.sharedInstance.sendExtendContractRequest { [unowned self] (success: Bool) -> () in
            
            self.isSendExtendContract = false
        }
    }
}
