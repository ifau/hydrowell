//
//  TextFieldCell.swift
//  gidrovel
//
//  Created by ifau on 07/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell
{
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var textField: UITextField!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
}
