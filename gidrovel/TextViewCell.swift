//
//  TextViewCell.swift
//  gidrovel
//
//  Created by ifau on 06/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class TextViewCell: UITableViewCell
{
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var textView: UITextView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
}
